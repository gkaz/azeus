# Azeus
This library allows to propagate user accounts and account change events from
one service (that acts as a user directory) to other services, thus keep the
accounts synchronized across multiple services.

[![pipeline status](https://gitlab.com/gkaz/azeus/badges/master/pipeline.svg)](https://gitlab.com/gkaz/azeus/-/commits/master)

## Authors
The project was initially written and being developed by "AZ Company Group" LLC
(https://www.gkaz.ru/) You can find the list of contributors in `AUTHORS` file.

## License
Azeus is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version. Please see `LICENSE` file for the terms of GNU General Public License.

## Dependencies
* php7.4-sqlite3

Build-time dependencies:
* php-xml (needed for PHPUnit tests)

## Configuration file example
```
[Global]

services_directory="services"
cache_directory="/var/cache/azeus/"

excluded_users[]="guest"
excluded_users[]="administrator"

[GitLab_service]
instance="https://gitlab.example.org"
token="secret-token"

[ownCloud_service]
instance="https://cloud.example.org"
user="azeus"
password="passw0rd"
default_groupname="moodle"
default_quota="512MB"
```

