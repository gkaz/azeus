<?php declare(strict_types=1);

include_once "azeus/Config.php";
include_once "azeus/Service.php";
include_once "azeus/Logger.php";
include_once "tests/services/Service1_service.php";

use PHPUnit\Framework\TestCase;
use \azeus\Config;
use \azeus\Service;
use function \azeus\vcard_get_property;
use \azeus\Logger;
use \azeus\Azeus_exception;
use JeroenDesloovere\VCard\VCard;

final class SessionTest extends TestCase {
    public function test_create_session(): void {
        $config = new Config('./tests/test_config.ini');
        $config->load();
        $service_name = 'Service1';
        $service_description = 'Some service';
        $logger = new Logger("test-logger");
        $service = new Service1_service($service_name, $logger, $config);
        $this->assertEquals(
            $service->get_name(),
            $service_name
        );
        $this->assertEquals(
            $service->get_description(),
            $service_description
        );
        $this->assertEquals(
            $service->get_option_value('instance'),
            $config->get($service_name . '_service', 'instance')
        );
    }

    public function test_vcard_get_property(): void {
        $vcard = new VCard();
        $vcard->addName("Pupkin", "Vasiliy");
        $this->assertEquals(
            vcard_get_property($vcard, "FN;CHARSET=utf-8"),
            "Vasiliy Pupkin"
        );
    }

    public function test_vcard_get_property_error(): void {
        $vcard = new VCard();
        $vcard->addName("Pupkin", "Vasiliy");
        $this->expectException(Azeus_exception::class);
        vcard_get_property($vcard, "missing-key");
    }
}
