<?php declare(strict_types=1);

include_once "azeus/Logger_interface.php";

use \azeus\Logger_interface;

class Dummy_logger implements Logger_interface {
    public function __construct(string $name, int $priority_threshold = LOG_DEBUG) {
        // Do nothing.
    }
    public function set_verbosity(int $priority_threshold) : void {
        // Do nothing.
    }
    public function log(int $priority, string $message) : void {
        // Do nothing.
    }
}

