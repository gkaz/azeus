<?php declare(strict_types=1);

include_once "azeus/Config.php";

use PHPUnit\Framework\TestCase;
use \azeus\Config;

final class ConfigTest extends TestCase {
    public function test_load_config(): void {
        $config = new Config('./tests/test_config.ini');
        $config->load();
        $this->assertEquals(
            $config->get('Global', 'services_directory'),
            'tests/services'
        );
        $this->assertEquals(
            $config->get('Global', 'excluded_users'),
            [ 'guest', 'administrator' ]
        );
    }
}
