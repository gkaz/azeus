<?php declare(strict_types=1);

include_once "azeus/Cache.php";

use PHPUnit\Framework\TestCase;
use \azeus\Cache;

final class CacheTest extends TestCase {
    private $cache;

    protected function setUp() : void {
        $this->cache = new Cache("tests/", new Dummy_logger(""));
    }

    public function test_open() : void {
        $result = $this->cache->open();
        $this->assertTrue($result);
    }

    public function test_update_new_user() : void {
        $this->cache->open();

        $result = $this->cache->update("alice", "qwerty");
        $this->assertTrue($result);
    }

    public function test_update_cache_hit() : void {
        $this->cache->open();

        $result = $this->cache->update("alice", "qwerty");
        $this->assertTrue($result);

        $result = $this->cache->update("alice", "qwerty");
        $this->assertFalse($result);
    }

    public function test_update_cache_miss() : void {
        $this->cache->open();

        $result = $this->cache->update("alice", "qwerty");
        $this->assertTrue($result);

        $result = $this->cache->update("alice", "qwerty123");
        $this->assertTrue($result);
    }

    public function test_update_second_user() : void {
        $this->cache->open();

        $result = $this->cache->update("alice", "qwerty");
        $this->assertTrue($result);

        $result = $this->cache->update("bob", "qwerty123");
        $this->assertTrue($result);
    }

    protected function tearDown() : void {
        $this->cache->delete();
    }
}
