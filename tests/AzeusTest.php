<?php declare(strict_types=1);

include_once "azeus/Config.php";
include_once "azeus/Azeus.php";
include_once "Dummy_logger.php";

use PHPUnit\Framework\TestCase;
use \azeus\Azeus;
use \azeus\Service;
use \azeus\Config;
use JeroenDesloovere\VCard\VCard;

final class AzeusTest extends TestCase {
    public function test_set_and_get_logger(): void {
        $config = new Config('./tests/test_config.ini');
        $config->load();
        $logger = new Dummy_logger("");
        $azeus = new Azeus($logger, $config);
        $this->assertEquals(
            $azeus->get_logger(),
            $logger
        );

        $logger2 = new Dummy_logger("");
        $azeus->set_logger($logger);
        $this->assertEquals(
            $azeus->get_logger(),
            $logger2
        );
    }

    public function test_load_service(): void {
        $config = new Config('./tests/test_config.ini');
        $config->load();
        $logger = new Dummy_logger("");
        $azeus = new Azeus($logger, $config);
        $azeus->load_service('Service2');
        $this->assertEquals(
            $azeus->get_service('Service2')->get_name(),
            'Service2'
        );
    }

    public function test_load_services(): void {
        $config = new Config('./tests/test_config.ini');
        $config->load();
        $logger = new Dummy_logger("");
        $azeus = new Azeus($logger, $config);
        $azeus->load_services();
        $this->assertEquals(
            $azeus->get_service('Service1')->get_name(),
            'Service1'
        );
        $this->assertEquals(
            $azeus->get_service('Service2')->get_name(),
            'Service2'
        );
    }

    public function test_propagate(): void {
        $login    = "alice";
        $password = "passw0rd";
        $config   = new Config('./tests/test_config.ini');
        $config->load();
        $logger = new Dummy_logger("");
        $azeus = new Azeus($logger, $config);
        $vcard = new VCard();
        $service = $this->createMock(Service::class);

        $service->expects($this->once())
                ->method("get_name")
                ->willReturn("Service");
        $service->expects($this->once())
                ->method("update")
                ->with($login, $password, $vcard);

        $azeus->add_service($service);
        $azeus->propagate($login, $password, $vcard);
    }

    protected function tearDown() : void {
        unlink('cache.db');
    }
}
