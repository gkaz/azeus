<?php

/**
 * This file contains a common Azeus configuration interface.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

namespace azeus;

/**
 * Common Azeus configuration interface.
 */
interface Config_interface {
    /**
     * The main class constructor. Note that the constructor does not load
     * contents of the file; you should call 'load' method before using
     * configuration values.
     *
     * @see Config_interface::load
     *
     * @param string $file_name Config file name.
     */
    public function __construct($file_name);

    /**
     * Load configuration from the file.
     *
     * @api
     */
    public function load();

    /**
     * Get a configuration value by the key.
     *
     * @api
     * @param string $section A section to lookup.
     * @param string $key     A key to lookup.
     * @return string A key value.
     */
    public function get($section, $key);

    /**
     * Set a configuration value.
     *
     * @api
     * @param string $section A configuration section to use.
     * @param string $key A key to set.
     * @param string $value A value to set.
     */
    public function set($section, $key, $value);
}

?>
