<?php

/**
 * This file contains definition of an abstract Azeus service.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

declare(strict_types = 1);

namespace azeus;

require_once(dirname(__FILE__) . "/Service_interface.php");
require_once(dirname(__FILE__) . "/Azeus_exception.php");

/**
 * @param VCard $vcard VCard instance to use.
 * @param string $key  Property key to search.
 * @return string Property value.
 * @throws Azeus_exception if no property found.
 */
function vcard_get_property($vcard, $key) {
    $properties = $vcard->getProperties();
    foreach ($properties as $property) {
        if ( ($property['key'] === $key) && ($property['value'] !== '') ) {
            return $property['value'];
        }
    }
    throw new Azeus_exception("Could not locate property: " . $key);
}

/**
 * Abstract service that should be used as a base for Azeus services.
 */
abstract class Service implements Service_interface {
    /**
     * @var Configuration instance.
     *
     * @see Config
     */
    private $config;

    /**
     * @var string Service name (without '_service'.)
     */
    protected $name;

    /**
     * @var string Service description.
     */
    protected $description;

    /**
     * @var Logger_interface A Logger_interface implementation instance.
     */
    protected $logger;

    /**
     * Default service constructor.
     *
     * @api
     * @param string $name Service name.
     * @param Logger_interface $logger Azeus logger instance.
     * @param Config_interface $config Azeus config instance.
     */
    public function __construct(string $name,
                                Logger_interface $logger,
                                Config_interface $config) {
        $this->name        = $name;
        $this->logger      = $logger;
        $this->config      = $config;
        $this->description = $this->get_option_value('service_description');
    }

    /**
     * Get value of a service option.
     *
     * @api
     * @param string $key An option name.
     * @return string Option value.
     */
    public function get_option_value(string $key) {
        return $this->config->get($this->name . '_service', $key);
    }

    /**
     * Get the service name.
     *
     * @api
     * @return string Service name.
     */
    public function get_name() : string {
        return $this->name;
    }

    /**
     * Get the service description.
     *
     * @return string Service description.
     */
    public function get_description() : string {
        return $this->description;
    }
}

?>
