<?php

/**
 * This file contains a declaration of an interface for classes that allow to
 * cache synchronization results.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

namespace azeus;

interface Cache_interface {

    /**
     * The class constructor.
     *
     * @param string $cache_directory A directory to store the cache.
     * @param Logger_interface $logger A logger instance to use.
     *
     * @api
     */
    public function __construct($cache_directory, $logger);

    /**
     * Open the cache.
     *
     * @return bool true if cache is opened successfully, false otherwise.
     *
     * @api
     */
    public function open();

    /**
     * Close the cache.
     *
     * @api
     */
    public function close();

    /**
     * Delete the cache.
     *
     * @return bool true if the cache was successfully removed, false otherwise.
     *
     * @api
     */
    public function delete();

    /**
     * Update a cache record for the given username and a password. The
     * procedure updates the cache only when it is not in the actual state.
     *
     * @param string $username Username to cache.
     * @param string $password Password to cache.
     * @return bool true if the record was updated, false if the record was
     *     not updated.
     * @api
     */
    public function update($username, $password);
}

?>
