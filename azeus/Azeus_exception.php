<?php

/**
 * This file contains declaration of Azeus exception class.
 */

namespace azeus;

/**
 * This class describes a generic Azeus exception.
 */
class Azeus_exception extends \Exception {

}

?>
