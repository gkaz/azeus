<?php

declare(strict_types = 1);

namespace azeus;

require_once(dirname(__FILE__) . "/Logger_interface.php");

/**
 * The default logger for Azeus that logs all messages to syslog.
 */
class Logger implements Logger_interface {
    /**
     * @var int Maximum priority that of messages that get logger.
     */
    private $priority_threshold;

    public function __construct(string $name, int $priority_threshold = LOG_DEBUG) {
        $this->priority_threshold = $priority_threshold;
        openlog($name, LOG_PID | LOG_PERROR, LOG_LOCAL0);
    }

    public function __destruct() {
        closelog();
    }

    /**
     * @copydoc Logger_interface::set_verbosity(int)
     */
    public function set_verbosity(int $priority_threshold) : void {
        $this->priority_threshold = $priority_threshold;
    }

    /**
     * @copydoc \azeus\Logger_interface::log(int, string)
     */
    public function log(int $priority, string $message) : void {
        if ($priority <= $this->priority_threshold) {
            syslog($priority, $message);
        }
    }
}

?>
