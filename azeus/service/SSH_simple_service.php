<?php

/**
 * Azeus service that allows to connect to remote services by means of SSH
 * protocol. This variant just calls 'ssh' to execute a command on a remote
 * side.
 *
 * Requirements:
 *   - openssh client
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

namespace azeus\service;

require_once(dirname(__FILE__) . "/../Service.php");
require_once(dirname(__FILE__) . "/../Azeus_exception.php");

use \azeus\Service;
use \azeus\Azeus_exception;

/**
 *  Azeus SSH service.
 */
abstract class SSH_simple_service extends Service {
    /**
     * @var string SSH host.
     */
    private $ssh_host;

    /**
     * @var int SSH port.
     */
    private $ssh_port;

    /**
     * @var string SSH user.
     */

    private $ssh_auth_user;

    /**
     * @var string Path to a public key.
     */
    private $ssh_auth_pub;

    /**
     * @var string Path to a private key.
     */
    private $ssh_auth_priv;

    /**
     * @var SSH connection.
     */
    private $ssh_connection;

    /**
     * @var string SSH server fingerprint.
     */
    private $ssh_server_fp;

    /**
     * Class constructor.
     *
     * @param string $name Service name.
     * @param Logger_interface $logger Azeus logger instance.
     * @param Config_interface $config Azeus config instance.
     */
    public function __construct($name, $logger, $config) {
        parent::__construct($name, $logger, $config);
        $this->ssh_host      = $this->get_option_value('ssh_host');
        $this->ssh_port      = $this->get_option_value('ssh_port');
        $this->ssh_auth_user = $this->get_option_value('ssh_auth_user');
        $this->ssh_auth_pub  = $this->get_option_value('ssh_auth_pub');
        $this->ssh_auth_priv = $this->get_option_value('ssh_auth_priv');
        $this->ssh_server_fp = $this->get_option_value('ssh_server_fp');
    }

    /**
     * Execute a command on the remote host.
     *
     * @param string $command A shell command to execute;
     * @param array $args Command arguments;
     * @return string Command output.
     * @throws Azeus_exception on errors.
     */
    protected function exec($command, $args) {
        $command = 'ssh '
                 . ' -p ' . $this->ssh_port
                 . ' -i ' . $this->ssh_auth_priv
                 . ' ' . $this->ssh_auth_user . '@' . $this->ssh_host
                 . ' ' . $command;

        foreach ($args as $arg) {
            $command .= ' "' . escapeshellarg($arg) . '" ';
        }

        $output = '';
        $return_value = 0;
        exec($command, $output, $return_value);
        if ($return_value != 0) {
            throw new Azeus_exception(
                'SSH command failed'
            );
        }

        return $output;
    }
}

?>
