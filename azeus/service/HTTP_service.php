<?php

/**
 * Azeus service that allows to connect by means of HTTP protocol.
 *
 * Requirements:
 *   - php-curl
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

declare(strict_types = 1);

namespace azeus\service;

require_once(dirname(__FILE__) . "/../Service.php");
require_once(dirname(__FILE__) . "/../Azeus_exception.php");

use \azeus\Service;
use \azeus\Azeus_exception;

/**
 *  Azeus HTTP service.
 */
abstract class HTTP_service extends Service {
    /**
     * @var string Server URL.
     */
    private $server;

    /**
     * @var CURL instance.
     */
    private $curl;

    /**
     * @var Custom HTTP(S) headers.
     */
    private $http_headers;

    /**
     * Class constructor.  Creates new CURL instance.
     *
     * @param string $name Service name.
     * @param Logger_interface $logger Azeus logger instance.
     * @param Config_interface $config Azeus config instance.
     */
    public function __construct($name, $logger, $config) {
        parent::__construct($name, $logger, $config);
        $this->server = $this->get_option_value("server");
        $this->curl = curl_init();
        $this->set_opt(CURLOPT_RETURNTRANSFER, true);
        $this->set_opt(CURLOPT_CONNECTTIMEOUT,
                       (int) $this->get_option_value("curl_connect_timeout"));
        $this->set_opt(CURLOPT_TIMEOUT,
                       (int) $this->get_option_value("curl_timeout"));
        $this->set_debug_mode($this->get_option_value("debug_mode"));
        $this->http_headers = [ 'Content-Type: application/json' ];
    }

    /**
     * Class destructor.  It closes the current CURL session.
     */
    public function __destruct() {
        curl_close($this->curl);
    }

    /**
     * Set custom HTTP(S) headers.
     */
    public function set_headers($headers) {
        $this->http_headers = $headers;
    }

    /**
     * Get Curl handle.
     *
     * @return mixed Curl handle.
     */
    protected function get_curl() {
        return $this->curl;
    }

    /**
     * Set Curl option.
     *
     * @param int $option Option to set.
     * @param $value Value to set.
     * @return void
     */
    protected function set_opt(int $option, $value): void {
        curl_setopt($this->curl, $option, $value);
    }

    /**
     *
     * @return string HTTP server link.
     */
    protected function get_server() {
        return $this->server;
    }

    /**
     * Get domain name from the server URL.
     *
     * @return string Domain name.
     */
    protected function get_domain() {
        return parse_url($this->server)['host'];
    }

    /**
     * Make an HTTP POST request.
     *
     * @param string $resource A resource on the server to use.
     * @param array  $data A data array to post.
     * @param array  $params Request parameters (optional.)
     * @return HTTP response from the server.
     */
    protected function post($resource, $data, $params = []) {
        if (! empty($params)) {
            $params = '?' . join("&", array_map(function ($key, $value){
                return $key . '=' . $value;
            }, array_keys($params), $params));
        } else {
            $params = '';
        }
        $this->set_opt(CURLOPT_CUSTOMREQUEST, "POST");
        $this->set_opt(CURLOPT_URL,
                    $this->server . $resource . $params);
        $this->set_opt(CURLOPT_POSTFIELDS, json_encode($data));
        $this->set_opt(CURLOPT_HTTPHEADER, $this->http_headers);
        $this->set_opt(CURLOPT_POST, 1);
        return curl_exec($this->curl);
    }

    /**
     * Get server port from the URL.
     *
     * @return Port number;
     */
    protected function get_port() {
        return parse_url($this->server)['port'];
    }

    /**
     * Set Curl debug mode to $value.
     *
     * @param boolean $is_enabled Is debug mode enabled?
     */
    protected function set_debug_mode($is_enabled) {
        $this->set_opt(CURLOPT_VERBOSE, $is_enabled);
    }

      /**
     * Make an HTTP GET request.
     *
     * @param string $resource A resource on the server to use.
     * @param array  $params Request parameters (optional.)
     * @return HTTP response from the server.
     */
    protected function get($resource, $params = []) {
        $this->set_opt(CURLOPT_HEADER, 0);
        $this->set_opt(CURLOPT_POST, 0);
        $this->set_opt(CURLOPT_CUSTOMREQUEST, "GET");
        if (! empty($params)) {
            $params = '?' . join("&", array_map(function ($key, $value){
                return $key . '=' . $value;
            }, array_keys($params), $params));
        } else {
            $params = '';
        }
        $this->set_opt(CURLOPT_HTTPHEADER, $this->http_headers);
        $this->set_opt(CURLOPT_URL, $this->server . $resource . $params);
        return curl_exec($this->curl);
    }

    /**
     * Make an HTTP PUT request.
     *
     * @param string $resource A resource on the server to use.
     * @param array  $data A data array to put.
     * @param array  $params Request parameters (optional.)
     * @return HTTP response from the server.
     */
    protected function put($resource, $data, $params = []) {
        $this->set_opt(CURLOPT_HEADER, 0);
        $this->set_opt(CURLOPT_CUSTOMREQUEST, "PUT");
        if (! empty($params)) {
            $params = '?' . join("&", array_map(function ($key, $value){
                return $key . '=' . $value;
            }, array_keys($params), $params));
        } else {
            $params = '';
        }

        $this->set_opt(CURLOPT_URL, $this->server . $resource . $params);
        $this->set_opt(CURLOPT_POSTFIELDS, json_encode($data));
        $this->set_opt(CURLOPT_HTTPHEADER, $this->http_headers);
        return curl_exec($this->curl);
    }
}

?>
