# HTTP service

## Requirements
- php-curl

## Config example
```
[My_HTTP_service]
server=https://example.org
curl_connect_timeout=10
curl_timeout=30
debug_mode=false
```
