# SSH service

## Requirements
- php-ssh2
- libssh2 1.9 or later, compiled with OpenSSL cryptography library.

Note that Ubuntu 19.10 is shipped with libssh2 1.8 compiled with Libgcrypt, and
this service will likely not work.

## Service example
```
<?php

require_once(dirname(__FILE__) . "/../azeus/service/SSH_service.php");

use \azeus\service\SSH_service;

class My_SSH_service extends SSH_service {
    public function update($login, $password, $email) {
        $this->connect();
        $this->exec("useradd -c '$email' -m -p '$password' $login");
        $this->disconnect();
    }
}

?>
```

## Config example
```
[My_SSH_service]
service_description="My SSH service"
ssh_host="example"
ssh_port=22
ssh_auth_user="alice"
ssh_auth_pub="~/.ssh/id_rsa.pub"
ssh_auth_priv="~/.ssh/id_rsa"
ssh_server_fp="key-fingerprint"
```

