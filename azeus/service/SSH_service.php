<?php

/**
 * Azeus service that allows to connect to remote services by means of HTTP(S)
 * protocol.
 *
 * Requirements:
 *   - php-ssh2
 *   - libssh2 1.9 or later, compiled with OpenSSL cryptography library.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

namespace azeus\service;

require_once(dirname(__FILE__) . "/../Service.php");
require_once(dirname(__FILE__) . "/../Azeus_exception.php");

use \azeus\Service;
use \azeus\Azeus_exception;

/**
 *  Azeus SSH service.
 */
abstract class SSH_service extends Service {
    /**
     * @var string SSH host.
     */
    private $ssh_host;

    /**
     * @var int SSH port.
     */
    private $ssh_port;

    /**
     * @var string SSH user.
     */

    private $ssh_auth_user;

    /**
     * @var string Path to a public key.
     */
    private $ssh_auth_pub;

    /**
     * @var string Path to a private key.
     */
    private $ssh_auth_priv;

    /**
     * @var SSH connection.
     */
    private $ssh_connection;

    /**
     * @var string SSH server fingerprint.
     */
    private $ssh_server_fp;

    /**
     * Class constructor.
     *
     * @param string $name Service name.
     * @param Logger_interface $logger Azeus logger instance.
     * @param Config_interface $config Azeus config instance.
     */
    public function __construct($name, $logger, $config) {
        parent::__construct($name, $logger, $config);
        $this->ssh_host      = $this->get_option_value('ssh_host');
        $this->ssh_port      = $this->get_option_value('ssh_port');
        $this->ssh_auth_user = $this->get_option_value('ssh_auth_user');
        $this->ssh_auth_pub  = $this->get_option_value('ssh_auth_pub');
        $this->ssh_auth_priv = $this->get_option_value('ssh_auth_priv');
        $this->ssh_server_fp = $this->get_option_value('ssh_server_fp');
    }

    /**
     * Connect to the server.
     *
     * @throws Azeus_exception on errors.
     */
    protected function connect() {
        $this->ssh_connection = ssh2_connect($this->ssh_host, $this->ssh_port,
                                             [ 'hostkey' => 'ssh-rsa' ]);
        if (! $this->ssh_connection) {
            throw new Azeus_exception("Could not connect: "
                                      . $this->ssh_auth_user
                                      . "@" . $this->ssh_host . ":" . $this->ssh_port);
        }

        $fingerprint = ssh2_fingerprint(
            $this->ssh_connection,
            SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX
        );

        if (strcmp($this->ssh_server_fp, $fingerprint) !== 0) {
            throw new Azeus_exception(
                "Server fingerprint does not match the expected value (expected "
                . "'" . $this->ssh_server_fp . "', got '" . $fingerprint . "')"
            );
        }

        if (! ssh2_auth_pubkey_file($this->ssh_connection,
                                    $this->ssh_auth_user,
                                    $this->ssh_auth_pub,
                                    $this->ssh_auth_priv)) {
            throw new Azeus_exception(
                "Authentication rejected by the server"
            );
        }
    }

    /**
     * Disconnect from the remote host.
     *
     * @param string $command A shell command to execute;
     * @return string Command output.
     * @throws Azeus_exception on errors.
     */
    protected function disconnect() {
        $this->exec('echo "EXITING" && exit;');
        $this->ssh_connection = null;
    }

    /**
     * Execute a command on the remote host.
     *
     * @param string $command A shell command to execute;
     * @return string Command output.
     * @throws Azeus_exception on errors.
     */
    protected function exec($command) {
        $stream = ssh2_exec($this->ssh_connection, $command);
        if (! $stream) {
            throw new Azeus_exception(
                'SSH command failed'
            );
        }

        stream_set_blocking($stream, true);

        $data = "";

        while ($buffer = fread($stream, 4096)) {
            $data .= $buffer;
        }

        fclose($stream);
        return $data;
    }
}

?>
