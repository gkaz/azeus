<?php

/**
 * This file contains a declaration of class that allows to cache
 * synchronization results.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

namespace azeus;

require_once(dirname(__FILE__) . "/Cache_interface.php");

class Cache implements Cache_interface {
    /**
     * @var string Synchronization cache directory path.
     */
    private $cache_dir;

    /**
     * @var string Synchronization cache database file path..
     */
    private $cache_db_file;

    /**
     * @var SQLite3 A database instance.
     */
    private $sqlite_handle;

    /**
     * @var Logger_interface A logger instance.
     */
    private $logger;

    public function __construct($cache_directory, $logger) {
        $this->cache_dir = $cache_directory;
        $this->logger    = $logger;
    }

    private function db_create() {
        $sql = <<<EOF
             CREATE TABLE Cache(
                 Username     TEXT UNIQUE,
                 Hash         TEXT,
                 Last_updated INT);
             EOF;
        $ret = $this->sqlite_handle->exec($sql);
        if (! $ret) {
            $this->logger(LOG_ERR, "Could not create a database.");
        }
    }

    public function delete() {
        if (! unlink($this->cache_db_file)) {
            $this->logger->log(LOG_ERR,
                               "Could not delete the cache database: "
                               . $this->cache_db_file);
            return false;
        } else {
            return true;
        }
    }

    public function open() {
        if (file_exists($this->cache_dir) == false) {
            mkdir($this->cache_dir, 0700);
        }

        if (is_writable($this->cache_dir) == false) {
            $this->logger->log(LOG_ERR,
                               $this->cache_dir
                               . " is not writable; cache is disabled");
            return false;
        }

        $this->cache_db_file = $this->cache_dir . "/cache.db";
        if (file_exists($this->cache_db_file)) {
            $this->sqlite_handle = new \SQLite3($this->cache_db_file);
        } else {
            $this->sqlite_handle = new \SQLite3($this->cache_db_file);
            $this->db_create();
        }

        return true;
    }

    public function close() {
        $this->sqlite_handle->close();
    }

    private function db_insert($username, $hash) {
        $sql = <<<EOF
            INSERT INTO Cache (Username, Hash, Last_updated)
            VALUES (:username, :hash, :last_updated);
        EOF;

        $statement = $this->sqlite_handle->prepare($sql);
        $statement->bindValue(":id",        0);
        $statement->bindValue(":username",  $username);
        $statement->bindValue(":hash",      $hash);
        $statement->bindValue(":last_updated", (new \DateTime())->getTimestamp());
        $statement->execute();
    }

    private function db_update($username, $hash) {
        $sql = <<<EOF
            UPDATE Cache SET Hash = :hash WHERE Username = :username;
        EOF;
        $statement = $this->sqlite_handle->prepare($sql);
        $statement->bindValue(":hash",     $hash);
        $statement->bindValue(":username", $username);
        $statement->execute();
    }

    public function update($username, $password) : bool {
        $hash = hash('md5', $username . ":" . $password);
        $query = "SELECT Username, Hash, Last_updated"
               . " FROM Cache"
               . " WHERE Username = '$username';";
        $result = $this->sqlite_handle->querySingle($query, true);
        if ($result == false) {
            $this->db_insert($username, $hash);
            return true;
        } else {
            if (! empty($result)) {
                if ($result['Hash'] == $hash) {
                    return false;
                } else {
                    $this->db_update($username, $hash);
                    return true;
                }
            } else {
                $this->db_insert($username, $hash);
                return true;
            }
        }
    }
}

?>
