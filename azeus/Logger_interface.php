<?php

/**
 * Common Logger interface.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

declare(strict_types = 1);

namespace azeus;

/**
 * This interface describes a logger for Azeus. All loggers must implement
 * the interface.
 */
interface Logger_interface {
    /**
     * Constructor for the class.
     *
     * @param string $name Application (service) name.
     * @param int $priority_threshold Maximum log priority.
     */
    public function __construct(string $name, int $priority_threshold = LOG_DEBUG);

    /**
     * All messages that above the specified priority will be discarded.
     *
     * @api
     * @param int $priority_threshold Maximum log priority.
     *     (see https://www.php.net/manual/en/function.syslog.php )
     */
    public function set_verbosity(int $priority_threshold) : void;

    /**
     * Log message with the specified priority.
     *
     * @api
     * @param int $priority Message priority as for syslog
     *     (see https://www.php.net/manual/en/function.syslog.php )
     * @param string $message A message to log.
     */
    public function log(int $priority, string $message) : void;
}

?>
