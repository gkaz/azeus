<?php

/**
 * Service interface.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

declare(strict_types = 1);

namespace azeus;

use JeroenDesloovere\VCard\VCard;

/**
 * Service interface.
 */
interface Service_interface {

    /**
     * The main class constructor.
     *
     * @api
     * @param string $name Name of the service.
     * @param Logger_interface $logger A logger instance.
     * @param Config_interface $config A configuration.
     */
    public function __construct(string $name,
                                Logger_interface $logger,
                                Config_interface $config);

    /**
     * Returns the service name.
     *
     * @api
     * @return string Service name.
     */
    public function get_name() : string;

    /**
     * Get description of the service.
     *
     * @api
     * @return string Service description.
     */
    public function get_description() : string;

    /**
     * Update user credentials.
     *
     * @api
     * @param string $login
     * @param string $password
     * @param VCard $vcard
     */
    public function update(string $login, string $password, VCard $vcard) : void;
}

?>
