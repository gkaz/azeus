<?php

/**
 * This file contains a declaration of class that allows to work with INI
 * configuration files.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

namespace azeus;

require_once(dirname(__FILE__) . "/Config_interface.php");

/**
 * Azeus configuration.
 */
class Config implements Config_interface {
    /**
     * @var string Name of the configuration file.
     */
    private $file_name;

    /**
     * @var array A parsed contents of the configuration file.
     */
    private $config;

    /**
     * @inherit Config_interface::__construct(string)
     */
    public function __construct($file_name) {
        $this->file_name = $file_name;
    }

    /**
     * @copydoc Config_interface::load()
     */
    public function load() {
        $this->config = parse_ini_file($this->file_name, true);
    }

    /**
     * @copydoc Config_interface::get(string, string)
     */
    public function get($section, $key) {
        return $this->config[$section][$key];
    }

    /**
     * @copydoc Config_interface::set(string, string, string)
     */
    public function set($section, $key, $value) {
        $this->config[$section][$key] = $value;
    }
}

?>
