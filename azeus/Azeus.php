<?php
/**
 * Azeus implementation.
 *
 * @author Artyom V. Poptsov <a@gkaz.ru>
 */

namespace azeus;

require_once(dirname(__FILE__) . "/Config.php");
require_once(dirname(__FILE__) . "/Cache.php");
require_once(dirname(__FILE__) . "/Cache_interface.php");
require_once(dirname(__FILE__) . "/Service_interface.php");
require_once(dirname(__FILE__) . "/Azeus_exception.php");

use JeroenDesloovere\VCard\VCard;

/**
 * Predicate.  Check if a file name is a module name.
 *
 * @param string $file_name A file name to check.
 * @return boolean true if it is a module name, false otherwise.
 */
function is_service_name($file_name) {
    return preg_match('/.*_service.php$/', $file_name) > 0;
}

/**
 * Check if a file name begins with a '.' (dot.)
 *
 * @param string $file_name Name of the file to check.
 * @return boolean
 */
function is_dotfile($file_name) {
    return preg_match('/^\..*/', $file_name) > 0;
}

/**
 * Convert a file name to a service name.
 *
 * @param string $file_name A file name.
 * @return string Service name.
 */
function file_name_to_service_name($file_name) {
    preg_match('/(.*)_service.php$/', $file_name, $matches);
    return $matches[1];
}

/**
 * Moodle as Single Sign-On server. This class can be used to interact with
 * service modules.
 */
class Azeus {
    /**
     * @var array Services to sync with.
     */
    private $services;

    /**
     * @var string Services directory.
     */
    private $services_dir;

    /**
     * @var Config_interface Azeus configuration instance.
     */
    private $config;

    /**
     * @var Logger_interface Azeus logger implementation instance.
     */
    private $logger;

    /**
     * @var Cache_interface $cache Azeus cache.
     */
    private $cache;

    /**
     * The main constructor of the class.
     *
     * @param Logger_interface $logger A logger implementation instance.
     * @param Config $config Azeus configuration.
     */
    public function __construct($logger, $config) {
        $this->logger      = $logger;
        $this->config      = $config;
        $this->services_dir = $this->config->get('Global', 'services_directory');
        $this->cache        = new Cache($this->config->get('Global', 'cache_directory'),
                                        $logger);

        $this->cache->open();
    }

    /**
     * Load a service from a file.
     *
     * @api
     * @param string $service_name Name of the service (without '_service' suffix.)
     */
    public function load_service($service_name) {
        $this->logger->log(
            LOG_DEBUG,
            "load_service: Loading service from file '"
            . $this->services_dir . '/' . $service_name . '_service.php'
            . "' ..."
        );
        include_once($this->services_dir . '/' . $service_name . '_service.php');
        $this->logger->log(
            LOG_DEBUG,
            "load_service: Loading service from file '"
            . $this->services_dir . '/' . $service_name . '_service.php'
            . "' ... done"
        );
        $service_class = $service_name . '_service';
        $service = new $service_class($service_name, $this->logger, $this->config);
        $this->logger->log(
            LOG_DEBUG,
            "load_service: Service class '"
            . $service_class . "' loaded"
        );
        $this->services[$service_name] = $service;
    }

    /**
     * Load services from files.
     *
     * @api
     * @return void
     * @throws Azeus_exception on errors.
     */
    public function load_services() {
        $this->logger->log(
            LOG_DEBUG,
            "load_services: Loading services from '" . $this->services_dir . "' ..."
        );
        if ($handle = opendir($this->services_dir)) {
            while (false !== ($entry = readdir($handle))) {
                if (is_dotfile($entry)) {
                    continue;
                }

                if (! is_service_name($entry)) {
                    continue;
                }

                $this->logger->log(
                    LOG_DEBUG,
                    "load_services: Loading service '"
                    . $this->services_dir . "/" . $entry . "' ..."
                );

                $this->load_service(file_name_to_service_name($entry));

                $this->logger->log(
                    LOG_DEBUG,
                    "load_services: Loading service '"
                    . $this->services_dir . "/" . $entry . "' ... done"
                );
            }
            closedir($handle);
            $this->logger->log(
                LOG_DEBUG,
                "load_services: Loading services from '"
                . $this->services_dir . "' ... done"
            );
        } else {
            throw new Azeus_exception("Could not open services directory: "
                                           . $this->services_dir);
        }
    }

    /**
     * Get a service object by name.
     *
     * @api
     * @param string $name Service name to lookup.
     * @return Service A service instance.
     */
    public function get_service($name) {
        return $this->services[$name];
    }

    /**
     * Add an existing service.
     *
     * @api
     * @param Service $service A Service instance.
     */
    public function add_service($service) {
        $this->services[$service->get_name()] = $service;
    }

    /**
     * Set Azeus logger.
     *
     * @api
     * @param Logger_interface $logger A logger instance.
     */
    public function set_logger($logger) {
        $this->logger = $logger;
    }

    /**
     * Get the current Azeus logger.
     *
     * @api
     * @returns The current Azeus logger.
     */
    public function get_logger() {
        return $this->logger;
    }

    /**
     * Predicate.  Check if a user is excluded from the synchronization.
     *
     * @api
     * @param string $login A user login to check.
     * @return boolean
     */
    public function is_user_excluded($login) {
        $excluded_users = $this->config->get('Global', 'excluded_users');
        return in_array($login, $excluded_users);
    }

    /**
     * Propagate $login and $password across enabled services.
     *
     * The procedure implements caching so repeating requests do not propagating
     * through the services.
     *
     * @param string $login A login to propagate.
     * @param string $password A password to propagate.
     * @param JeroenDesloovere\VCard\VCard $vcard A user VCard to propagate.
     */
    public function propagate($login, $password, $vcard) {
        if ($this->is_user_excluded($login)) {
            $this->logger->log(
                LOG_DEBUG,
                "User '$login' is excluded from synchronization"
            );
            return;
        }

        if ($this->cache->update($login, $password)) {
            try {
                foreach ($this->services as $service_name => $service) {
                    $this->logger->log(LOG_DEBUG,
                                       "Syncing service $service_name ...");
                    $service->update($login, $password, $vcard);
                }
            } catch (\Exception $e) {
                $this->logger->log(LOG_ERR, $e->getMessage());
            }
        }
    }
}

?>
